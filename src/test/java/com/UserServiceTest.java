package com;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by me on 2016-01-05.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
public class UserServiceTest {

    @Autowired
    UserDao userDao;

    @Autowired
    UserService userService;
    List<User> users;

    @Before
    public void setUp() throws Exception {
        users = Arrays.asList(
                new User("oceanfog1", "krk", "1123", Level.BASIC, 49, 0)
                ,new User("oceanfog2", "krk", "1123", Level.BASIC, 50, 0)
                ,new User("oceanfog3", "krk", "1123", Level.SILVER, 60, 29)
                ,new User("oceanfog4", "krk", "1123", Level.SILVER, 60, 30)
                ,new User("oceanfog5", "krk", "1123", Level.GOLD, 100, 100)
        );
    }

    @Test
    public void testBean() throws Exception {
        assertNotNull(userService);
    }

    @Test
    public void testUpgradeLevels() throws Exception {

        userDao.deletAll();
        for(User user : users) userDao.add(user);

        userService.upgradeLevels();


    }
}