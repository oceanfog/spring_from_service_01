package com;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by me on 2016-01-02.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
public class UserDaoTest {

    @Autowired
    private UserDaoJdbc userDao2;
    private User user1, user2, user3;

    @Before
    public void setUp() throws Exception {
        this.user1 = new User("oceanfog", "krk", "1123", Level.BASIC, 10, 20);
        this.user2 = new User("oceanfog2", "krk2", "1124", Level.SILVER, 100, 200);
        this.user3 = new User("oceanfog3", "krk3", "1125", Level.GOLD, 1000, 2000);
    }

    @Test
    public void testBean() throws Exception {


    }

    @Test
    public void testAddAndGet() throws Exception {

        userDao2.deletAll();
        userDao2.add(user1);
        assertEquals(1, userDao2.getCount() );

        userDao2.add(user2);
        assertEquals(2, userDao2.getCount() );

        userDao2.add(user3);
        assertEquals(3, userDao2.getCount() );

        List<User> userList = userDao2.getAll();
        checkSame(user1, userList.get(0));
        checkSame(user2, userList.get(1));
        checkSame(user3, userList.get(2));
    }

    private void checkSame(User user1, User user2){
        assertEquals(user1.getId(), user2.getId());
        assertEquals(user1.getName(), user2.getName());
        assertEquals(user1.getPassword(), user2.getPassword());
        assertEquals(user1.getLevel(), user2.getLevel());
        assertEquals(user1.getLogin(), user2.getLogin());
        assertEquals(user1.getRecommend(), user2.getRecommend());
    }
}