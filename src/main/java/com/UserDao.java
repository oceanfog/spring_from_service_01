package com;

import java.util.List;

/**
 * Created by me on 2016-01-01.
 */
public interface UserDao {

    public User get(String id);
    public List<User> getAll();
    public void deletAll();
    public void update(User user);
    public void add(User user);
    public int getCount();
}
