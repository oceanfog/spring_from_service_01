package com;

/**
 * Created by me on 2016-01-01.
 */
public class User {
    private String id;
    private String name;
    private String password;
    private Level level;
    private int Login;
    private int Recommend;

    public User() {
    }

    public User(String id, String name, String password, Level level, int login, int recommend) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.level = level;
        Login = login;
        Recommend = recommend;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public int getLogin() {
        return Login;
    }

    public void setLogin(int login) {
        Login = login;
    }

    public int getRecommend() {
        return Recommend;
    }

    public void setRecommend(int recommend) {
        Recommend = recommend;
    }
}
