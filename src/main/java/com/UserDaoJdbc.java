package com;


import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Created by me on 2016-01-01.
 */
public class UserDaoJdbc implements UserDao {
    private JdbcTemplate jdbcTemplate;
    private Map<String, String> sqlMap;
    private RowMapper<User> userRowMapper;

    public UserDaoJdbc() {
        userRowMapper = new RowMapper<User>() {
            @Override
            public User mapRow(ResultSet rs, int rowNum) throws SQLException {
                User user = new User();
                user.setId(rs.getString("id"));
                user.setName(rs.getString("name"));
                user.setPassword(rs.getString("password"));
                user.setLevel(Level.valueOf(rs.getInt("level")));
                user.setLogin(rs.getInt("login"));
                user.setRecommend(rs.getInt("recommend"));
                return user;
            }
        };
    }

    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate();
        this.jdbcTemplate.setDataSource(dataSource);

    }



    @Override
    public User get(String id) {
        return jdbcTemplate.queryForObject(sqlMap.get("get"), new Object[] {id}, userRowMapper);
    }

    @Override
    public List<User> getAll() {
        return this.jdbcTemplate.query(sqlMap.get("getAll"), userRowMapper);
    }

    @Override
    public void deletAll() {
        this.jdbcTemplate.update(sqlMap.get("deleteAll"));
    }

    @Override
    public void update(User user) {

    }

    @Override
    public void add(User user) {
        this.jdbcTemplate.update(sqlMap.get("add"), user.getId(), user.getName(), user.getPassword()
                , user.getLevel().intValue(), user.getLogin(), user.getRecommend());
    }

    @Override
    public int getCount() {
        ResultSetExtractor<Integer> resultSetExtractor = new ResultSetExtractor<Integer>(){
            @Override
            public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
                rs.next();
                return rs.getInt("count(*)");
            }
        };
        return this.jdbcTemplate.query(sqlMap.get("getCount"), resultSetExtractor);

    }

    public void setSqlMap(Map sqlMap) {
        this.sqlMap = sqlMap;
    }


}
